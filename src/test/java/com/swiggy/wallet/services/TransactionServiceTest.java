package com.swiggy.wallet.services;

import com.swiggy.wallet.models.Transaction;
import com.swiggy.wallet.models.User;
import com.swiggy.wallet.models.Wallet;
import com.swiggy.wallet.repositories.TransactionRepository;
import com.swiggy.wallet.repositories.UserRepository;
import com.swiggy.wallet.repositories.WalletRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TransactionServiceTest {

    private static final int INITIAL_BALANCE = 1000;
    private static final int CREDIT_AMOUNT = 100;
    private static final int DEBIT_AMOUNT = 50;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private WalletRepository walletRepository;

    @AfterEach
    void clearRepositories() {
        transactionRepository.deleteAll();
    }

    @Test
    void expectTransactionToBeCreated() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);
        TransactionService transactionService = new TransactionService(walletService, transactionRepository);

        User user = userService.createUser();
        Wallet wallet = userService.createWallet(user.getId(), 1000);
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, 100);
        Transaction createdTransaction = transactionService.createTransaction(wallet.getId(), transaction);

        assertEquals(1, transactionRepository.count());
        assertEquals(transactionRepository.findById(createdTransaction.getId()).orElse(null), createdTransaction);
    }

    @Test
    void expectCreditTransactionToUpdateWalletBalance() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);
        TransactionService transactionService = new TransactionService(walletService, transactionRepository);

        User user = userService.createUser();
        Wallet wallet = userService.createWallet(user.getId(), INITIAL_BALANCE);
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, CREDIT_AMOUNT);
        transactionService.createTransaction(wallet.getId(), transaction);

        assertEquals(INITIAL_BALANCE + CREDIT_AMOUNT, walletRepository.findById(wallet.getId()).get().getBalance()) ;
    }

    @Test
    void expectDebitTransactionToUpdateWalletBalance() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);
        TransactionService transactionService = new TransactionService(walletService, transactionRepository);

        User user = userService.createUser();
        Wallet wallet = userService.createWallet(user.getId(), INITIAL_BALANCE);
        Transaction transaction = new Transaction(Transaction.Type.DEBIT, DEBIT_AMOUNT);
        transactionService.createTransaction(wallet.getId(), transaction);

        assertEquals(INITIAL_BALANCE - DEBIT_AMOUNT, walletRepository.findById(wallet.getId()).get().getBalance()) ;
    }

    @Test
    void expectToFetchRecentTransactions() {
        WalletService walletService = new WalletService(walletRepository);
        TransactionService transactionService = new TransactionService(walletService, transactionRepository);

        User user = new User();
        user.addWallet(new Wallet(1000));
        user = userRepository.save(user);
        Wallet wallet = user.getWallets().get(0);
        wallet.addTransaction(new Transaction(Transaction.Type.DEBIT, 100));
        wallet.addTransaction(new Transaction(Transaction.Type.CREDIT, 10));
        wallet = walletRepository.save(wallet);

//        assertEquals(otherTransaction, transactionService.getRecentTransactions(user.getId(), wallet.getId(), 2));
//        assertEquals(Arrays.asList(transaction, otherTransaction), transactionService.getRecentTransactions(user.getId(), wallet.getId(), 2));
//        assertEquals(Arrays.asList(transaction, otherTransaction), transactionService.getRecentTransactions(user.getId(), wallet.getId(), 10));
    }
}