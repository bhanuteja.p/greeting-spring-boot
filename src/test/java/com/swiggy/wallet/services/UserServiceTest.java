package com.swiggy.wallet.services;

import com.swiggy.wallet.models.User;
import com.swiggy.wallet.models.Wallet;
import com.swiggy.wallet.repositories.UserRepository;
import com.swiggy.wallet.repositories.WalletRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletRepository walletRepository;

    @AfterEach
    void clearRepositories() {
        userRepository.deleteAll();
        walletRepository.deleteAll();
    }

    @Test
    void expectUserToBeCreated() {
        UserService userService = new UserService(new WalletService(walletRepository), userRepository);

        User user = userService.createUser();

        assertEquals(1, userRepository.count());
        assertEquals(userRepository.findById(user.getId()).orElse(null), user);
    }

    @Test
    void expectAllUsersToBeFetched() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);

        User user = userService.createUser();
        User otherUser = userService.createUser();

        assertEquals(2, userRepository.count());
        assertEquals(Arrays.asList(user, otherUser), userService.getUsers());
    }

    @Test
    void expectToFetchUser() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);

        User user = userService.createUser();

        assertEquals(1, userRepository.count());
        assertEquals(Optional.of(user), userService.fetchUser(user.getId()));
    }

    @Test
    void expectNotToFetchUser() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);

        User user = userService.createUser();

        assertEquals(1, userRepository.count());
        assertEquals(Optional.empty(), userService.fetchUser(user.getId()+1));
    }

    @Test
    void expectToFetchUserWallets() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);

        User user = userService.createUser();
        Wallet wallet = userService.createWallet(user.getId(), 100);
        Wallet otherWallet = userService.createWallet(user.getId(), 1000);

        assertEquals(2, walletRepository.count());
        assertEquals(Arrays.asList(wallet, otherWallet), userService.getUserWallets(user.getId()));
    }

    @Test
    void expectToFetchUserWallet() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);

        User user = userService.createUser();
        Wallet wallet = userService.createWallet(user.getId(), 100);

        assertEquals(1, walletRepository.count());
        assertEquals(wallet, userService.getUserWallet(wallet.getId()));
    }

    @Test
    void expectToCreateWallet() {
        WalletService walletService = new WalletService(walletRepository);
        UserService userService = new UserService(walletService, userRepository);
        User user = userService.createUser();

        Wallet wallet = userService.createWallet(user.getId(), 1000);

        assertEquals(1, walletRepository.count());
        assertEquals(walletRepository.findById(wallet.getId()).orElse(null), wallet);
    }
}
