package com.swiggy.wallet.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testSerializationOfUser() throws JsonProcessingException {
        User user = new User();
        String json = objectMapper.writeValueAsString(user);
        assertEquals("{\"id\":0,\"wallets\":[]}", json);
    }

    @Test
    void testDeserializationOfUser() throws IOException {
        String userWallet = "{\"id\":0,\"wallets\":[]}";
        User user = objectMapper.readValue(userWallet, User.class);
        assertEquals(user, new User());
    }
}
