package com.swiggy.wallet.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WalletTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testSerializationOfWallet() throws JsonProcessingException {
        Wallet wallet = new Wallet(1000);
        String json = objectMapper.writeValueAsString(wallet);
        assertEquals("{\"id\":0,\"balance\":1000,\"transactions\":[]}", json);
    }

    @Test
    void testDeserializationOfWallet() throws IOException {
        String stringWallet = "{\"id\":0,\"balance\":1000}";
        Wallet wallet = objectMapper.readValue(stringWallet, Wallet.class);
        assertEquals(wallet, new Wallet(1000));
    }

    @Test
    void expectTheDebitTransactionToApplyToWallet() {
        Transaction transaction = new Transaction(Transaction.Type.DEBIT, 100);
        Wallet wallet = new Wallet(1000);

        wallet.applyTransaction(transaction);

        assertEquals(900, wallet.getBalance());
        assertTrue(wallet.getTransactions().contains(transaction));
    }

    @Test
    void expectTheCreditTransactionToApplyToWallet() {
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, 100);
        Wallet wallet = new Wallet(1000);

        wallet.applyTransaction(transaction);

        assertEquals(1100, wallet.getBalance());
        assertTrue(wallet.getTransactions().contains(transaction));
    }

    @Test
    void testNegativeBalanceValidation() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Wallet wallet = new Wallet(-1000);
        Set<ConstraintViolation<Wallet>> validate = validator.validate(wallet);
        assertEquals(validate.size(), 1);
    }

    @Test
    void expectToFetchRecentTransactions() {
        Wallet wallet = new Wallet(1000000);
        Transaction transaction = new Transaction(Transaction.Type.DEBIT, 100);
        Transaction otherTransaction = new Transaction(Transaction.Type.CREDIT, 10);

        wallet.addTransaction(transaction);
        wallet.addTransaction(otherTransaction);

        assertEquals(otherTransaction, wallet.getRecentTransactions(1).get(0));
        assertEquals(Arrays.asList(transaction, otherTransaction), wallet.getRecentTransactions(2));
        assertEquals(Arrays.asList(transaction, otherTransaction), wallet.getRecentTransactions(10));
    }
}
