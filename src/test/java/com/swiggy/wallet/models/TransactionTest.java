package com.swiggy.wallet.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TransactionTest {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testSerializationOfTransaction() throws JsonProcessingException {
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, 1000);
        String json = objectMapper.writeValueAsString(transaction);
        assertEquals("{\"id\":0,\"type\":\"CREDIT\",\"amount\":1000}", json);
    }

    @Test
    void testDeserializationOfTransaction() throws IOException {
        String transactionString = "{\"id\":0,\"type\":\"CREDIT\",\"amount\":1000}";
        Transaction transaction = objectMapper.readValue(transactionString, Transaction.class);
        assertEquals(transaction, new Transaction(Transaction.Type.CREDIT, 1000));
    }

    @Test
    void expectAmountToBeNegativeWhenDebit() {
        Transaction transaction = new Transaction(Transaction.Type.DEBIT, 1000);
        assertEquals(-1000, transaction.getAdjustedAmount());
    }

    @Test
    void testValidationWhenAmountIsGreaterThan10000() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, 10001);
        Set<ConstraintViolation<Transaction>> validate = validator.validate(transaction);
        assertEquals(validate.size(), 1);
    }
}
