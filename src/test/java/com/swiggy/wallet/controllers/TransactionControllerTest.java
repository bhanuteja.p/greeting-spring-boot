package com.swiggy.wallet.controllers;

import com.swiggy.wallet.models.Transaction;
import com.swiggy.wallet.services.TransactionService;
import com.swiggy.wallet.services.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolationException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private UserService userService;

    @Test
    void expectTransactionToBeCreated() throws Exception {
        Transaction transaction = new Transaction(Transaction.Type.CREDIT, 100);
        when(transactionService.createTransaction(eq(11L), any(Transaction.class))).thenReturn(transaction);
        String requestBody = "{\n" +
                "  \"type\": \"CREDIT\",\n" +
                "  \"amount\": \"100\"\n" +
                "}";

        MvcResult mvcResult = mockMvc.perform(
                post("/users/1/wallets/11/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.amount", Matchers.is(100)))
                .andExpect(jsonPath("$.type", Matchers.is("CREDIT")))
                .andExpect(jsonPath("$.id", Matchers.is(0)))
                .andReturn();

        verify(transactionService).createTransaction(eq(11L), any(Transaction.class));
    }

    @Test
    void expect400WhenAmountIsMoreThan10000() throws Exception {
        String requestBody = "{\n" +
                "  \"type\": \"CREDIT\",\n" +
                "  \"amount\": \"100001\"\n" +
                "}";

        mockMvc.perform(
                post("/users/1/wallets/11/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testGetRecentTransactions() throws Exception {
        List<Transaction> transactions = Arrays.asList(
                new Transaction(Transaction.Type.CREDIT, 100),
                new Transaction(Transaction.Type.DEBIT, 1000)
        );
//        when(transactionService.getRecentTransactions(1L, 11L, 10)).thenReturn(transactions);
        mockMvc.perform(
                get("/users/1/wallets/11/transactions?count=10"))
                .andExpect(status().isOk());

        verify(transactionService).getRecentTransactions(1L, 11L, 10);
    }
}
