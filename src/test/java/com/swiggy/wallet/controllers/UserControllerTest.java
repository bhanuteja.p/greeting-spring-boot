package com.swiggy.wallet.controllers;

import com.swiggy.wallet.models.User;
import com.swiggy.wallet.models.Wallet;
import com.swiggy.wallet.services.TransactionService;
import com.swiggy.wallet.services.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private TransactionService transactionService;

    @Test
    void testGetUsers() throws Exception {
        when(userService.getUsers())
                .thenReturn(
                        Arrays.asList(
                                new User(),
                                new User()
                        )
                );

        mockMvc.perform(
                get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));

        verify(userService).getUsers();
    }

    @Test
    void testGetUser() throws Exception {
        when(userService.fetchUser(1L)).thenReturn(Optional.of(new User()));

        mockMvc.perform(
                get("/users/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));

        verify(userService).fetchUser(1L);
    }

    @Test
    void testGetUser404() throws Exception {
        when(userService.fetchUser(1L)).thenReturn(Optional.empty());

        mockMvc.perform(
                get("/users/1"))
                .andExpect(status().isNotFound());

        verify(userService).fetchUser(1L);
    }

    @Test
    void testGetUserWallets() throws Exception {
        when(userService.getUserWallets(1L))
                .thenReturn(
                        Arrays.asList(
                                new Wallet(100),
                                new Wallet(101)
                        )
                );

        mockMvc.perform(
                get("/users/1/wallets").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));

        verify(userService).getUserWallets(1L);
    }

    @Test
    void testGetUserWallet() throws Exception {
        when(userService.getUserWallet(11L)).thenReturn(Optional.of(new Wallet(1101)));

        mockMvc.perform(
                get("/users/1/wallets/11").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.balance", Matchers.is(1101)));

        verify(userService).getUserWallet(11L);
    }

    @Test
    void testGetUserWallet404() throws Exception {
        when(userService.getUserWallet(11L)).thenReturn(Optional.empty());

        mockMvc.perform(
                get("/users/1/wallets/11").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(userService).getUserWallet(11L);
    }

    @Test
    void testCreateUser() throws Exception {
        when(userService.createUser()).thenReturn(new User());

        MvcResult mvcResult = mockMvc.perform(
                post("/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)))
                .andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains("\"id\":"));
        verify(userService).createUser();
    }

    @Test
    void testCreateWallet() throws Exception {
        when(userService.createWallet(1L, 1101)).thenReturn(new Wallet(1101));

        MvcResult mvcResult = mockMvc.perform(
                post("/users/1/wallets")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"balance\": \"1101\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.balance", Matchers.is(1101)))
                .andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains("\"id\":"));
        verify(userService).createWallet(1L, 1101);
    }

    @Test
    void expect400WhenAddingWalletWithNegativeBalance() throws Exception {
        mockMvc.perform(
                post("/users/1/wallets")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"balance\": \"-100\"}"))
                .andExpect(status().isBadRequest());
    }
}