package com.swiggy.wallet.controllers;

import com.swiggy.wallet.models.Transaction;
import com.swiggy.wallet.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TransactionController {

    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("/users/{userId}/wallets/{walletId}/transactions")
    Transaction createTransaction(@PathVariable Long walletId, @Valid @RequestBody Transaction transaction) {
        return transactionService.createTransaction(walletId, transaction);
    }

    @GetMapping("/users/{userId}/wallets/{walletId}/transactions")
    List<Transaction> getRecentTransactions(@PathVariable Long userId, @PathVariable Long walletId, @RequestParam(defaultValue = "10") int count) {
        return transactionService.getRecentTransactions(userId, walletId, count);
    }
}
