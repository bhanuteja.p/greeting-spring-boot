package com.swiggy.wallet.controllers;


import com.swiggy.wallet.models.User;
import com.swiggy.wallet.models.Wallet;
import com.swiggy.wallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    ResponseEntity<List<User>> getUsers() {
        List<User> users = userService.getUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    ResponseEntity getUser(@PathVariable Long id) {
        Optional<User> user = userService.fetchUser(id);
        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User Not Found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(user.get());
    }

    @GetMapping("/users/{id}/wallets")
    ResponseEntity<List<Wallet>> getUserWallets(@PathVariable Long id) {
        List<Wallet> wallets = userService.getUserWallets(id);
        return new ResponseEntity<>(wallets, HttpStatus.OK);
    }

    @GetMapping("/users/{id}/wallets/{walletId}")
    ResponseEntity getUserWallet(@PathVariable Long walletId) {
        Optional<Wallet> wallet = userService.getUserWallet(walletId);
        if (wallet.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Wallet Not Found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(wallet.get());
    }

    @PostMapping(path = "/users/{id}/wallets")
    ResponseEntity<Wallet> createWallet(@Valid @RequestBody Wallet wallet, @PathVariable Long id) {
        Wallet createdWallet = userService.createWallet(id, wallet.getBalance());
        return new ResponseEntity<>(createdWallet, HttpStatus.CREATED);
    }

    @PostMapping(path = "/users")
    ResponseEntity<User> createUser() {
        User user = userService.createUser();
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
