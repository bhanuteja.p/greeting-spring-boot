package com.swiggy.wallet.repositories;


import com.swiggy.wallet.models.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    @Override
    List<Transaction> findAll();

    List<Transaction> findByWalletIdOrderById(Long walletId, Pageable pageable);

}
