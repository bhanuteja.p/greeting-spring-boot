package com.swiggy.wallet.repositories;

import com.swiggy.wallet.models.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long> {

    @Override
    List<Wallet> findAll();
}
