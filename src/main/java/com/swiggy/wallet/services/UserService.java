package com.swiggy.wallet.services;

import com.swiggy.wallet.repositories.UserRepository;
import com.swiggy.wallet.models.User;
import com.swiggy.wallet.models.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private WalletService walletService;
    private UserRepository userRepository;


    @Autowired
    public UserService(WalletService walletService, UserRepository userRepository) {
        this.walletService = walletService;
        this.userRepository = userRepository;
    }

    public User createUser() {
        return userRepository.save(new User());
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> fetchUser(Long id) {
        return userRepository.findById(id);
    }

    public User getUser(Long id) {
        return fetchUser(id).get();
    }

    public List<Wallet> getUserWallets(Long id) {
        return getUser(id).getWallets();
    }

    public Optional<Wallet> getUserWallet(Long walletId) {
        return walletService.fetchWallet(walletId);
    }

    public Wallet createWallet(Long id, int balance) {
        Wallet wallet = new Wallet(balance);
        User user = this.getUser(id);
        wallet.setUser(user);
        wallet = walletService.createWallet(wallet);
        user.addWallet(wallet);
        userRepository.save(user);
        return user.getWallets().get(user.getWallets().size()-1);
    }
}