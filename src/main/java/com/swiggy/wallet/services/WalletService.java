package com.swiggy.wallet.services;

import com.swiggy.wallet.models.Transaction;
import com.swiggy.wallet.repositories.WalletRepository;
import com.swiggy.wallet.models.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class WalletService {

    private WalletRepository walletRepository;

    @Autowired
    public WalletService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    Optional<Wallet> fetchWallet(Long id) {
        return walletRepository.findById(id);
    }

    Wallet getWallet(Long id) {
        return walletRepository.findById(id).get();
    }

    Wallet createWallet(int balance) {
        return walletRepository.save(new Wallet(balance));
    }

    Wallet createWallet(Wallet wallet) {
        return walletRepository.save(wallet);
    }

    Transaction applyTransaction(Long walletId, Transaction transaction) {
        Wallet wallet = getWallet(walletId);
        transaction.setWallet(wallet);
        wallet.applyTransaction(transaction);
        wallet = walletRepository.save(wallet);
        int latestTransactionIndex = wallet.getTransactions().size() - 1;
        return wallet.getTransactions().get(latestTransactionIndex);
    }
}
