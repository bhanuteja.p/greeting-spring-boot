package com.swiggy.wallet.services;

import com.swiggy.wallet.models.Transaction;
import com.swiggy.wallet.models.Wallet;
import com.swiggy.wallet.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {

    private WalletService walletService;
    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(WalletService walletService, TransactionRepository transactionRepository) {
        this.walletService = walletService;
        this.transactionRepository = transactionRepository;
    }

    public Transaction createTransaction(Long walletId, Transaction transaction) {
        return walletService.applyTransaction(walletId, transaction);
    }

    public List<Transaction> getRecentTransactions(Long userId, Long walletId, int count) {
        Wallet wallet = walletService.getWallet(walletId);
        return transactionRepository.findByWalletIdOrderById(walletId, PageRequest.of(0, count, Sort.Direction.DESC));
    }
}
