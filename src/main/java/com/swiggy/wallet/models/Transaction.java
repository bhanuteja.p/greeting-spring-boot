package com.swiggy.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.Objects;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(position = 1)
    private Long id;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(position = 2)
    private Type type;

    @ApiModelProperty(position = 3)
    @Max(value = 10000, message = "Amount Should be less than or equal to 10000")
    private int amount;

    @ManyToOne
    @JsonIgnore
    @ApiModelProperty(position = 4)
    private Wallet wallet;

    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public Transaction(Type type, int amount) {
        this.id = 0L;
        this.type = type;
        this.amount = amount;
    }

    public Transaction(Type type, int amount, Wallet wallet) {
        this.id = 0L;
        this.type = type;
        this.amount = amount;
        this.wallet = wallet;
    }

    public Transaction() {
        this.id = 0L;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public int getAmount() {
        return amount;
    }

    public Long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    private boolean isDebit() {
        return type == Type.DEBIT;
    }

    int getAdjustedAmount() {
        return isDebit() ? -1 * amount : amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return amount == that.amount &&
                Objects.equals(id, that.id) &&
                type == that.type &&
                Objects.equals(wallet, that.wallet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, amount, wallet);
    }

    public enum Type {DEBIT, CREDIT}

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
