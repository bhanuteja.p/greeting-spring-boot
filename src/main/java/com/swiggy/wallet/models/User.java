package com.swiggy.wallet.models;


import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(position = 1)
    private Long id;

    @OneToMany(targetEntity = Wallet.class, cascade = CascadeType.ALL,mappedBy = "user")
    @ApiModelProperty(position = 2)
    private List<Wallet> wallets;

    public User() {
        this.id = 0L;
        this.wallets = new ArrayList<>();
    }

    User(Wallet wallet) {
        this.id = 0L;
        this.wallets = new ArrayList<>();
        this.wallets.add(wallet);
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public Long getId() {
        return id;
    }

    public void addWallet(Wallet wallet) {
        this.wallets.add(wallet);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", wallets=" + wallets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                wallets.equals(user.wallets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, wallets);
    }
}
