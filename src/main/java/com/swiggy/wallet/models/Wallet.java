package com.swiggy.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(position = 1, value = "api id")
    private Long id;

    @ApiModelProperty(position = 2, value = "BALANCE", required = true)

    @PositiveOrZero(message = "Balance should not be negative")
    private int balance;

    @ApiModelProperty(position = 3, value = "TRANSACTIONS LIST")
    @OneToMany(targetEntity = Transaction.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "wallet_id")
    private List<Transaction> transactions;

    @ManyToOne
    @JsonIgnore
    private User user;

    public Wallet() {
        this.id = 0L;
        this.transactions = new ArrayList<>();
    }

    public Wallet(int balance) {
        id = 0L;
        this.balance = balance;
        this.transactions = new ArrayList<>();
    }

    public Wallet(int balance, User user) {
        this.id = 0L;
        this.balance = balance;
        this.user = user;
    }

    public int getBalance() {
        return balance;
    }

    private void setBalance(int balance) {
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "id=" + id +
                ", balance=" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return balance == wallet.balance &&
                id.equals(wallet.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }

    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }

    public void applyTransaction(Transaction transaction) {
        addTransaction(transaction);
        this.setBalance(balance + transaction.getAdjustedAmount());
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Transaction> getRecentTransactions(int count) {
        int numberOfTransactions = transactions.size();
        int startIndex = Integer.max(numberOfTransactions - count, 0);
        return transactions.subList(startIndex, numberOfTransactions);
    }
}
